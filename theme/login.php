<?php

/**
 * Based roughly on wp-login.php @revision 19414
 * http://core.trac.wordpress.org/browser/trunk/wp-login.php?rev=19414
 */

global $Password_Protected, $error, $is_iphone;

/**
 * WP Shake JS
 */
if ( ! function_exists( 'wp_shake_js' ) ) {
	function wp_shake_js() {
		global $is_iphone;
		if ( $is_iphone ) {
			return;
		}
		?>
		<script type="text/javascript">
		addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
		function s(id,pos){g(id).left=pos+'px';}
		function g(id){return document.getElementById(id).style;}
		function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
		addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
		</script>
		<?php
	}
}

nocache_headers();
header( 'Content-Type: ' . get_bloginfo( 'html_type' ) . '; charset=' . get_bloginfo( 'charset' ) );

// Set a cookie now to see if they are supported by the browser.
setcookie( TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN );
if ( SITECOOKIEPATH != COOKIEPATH ) {
	setcookie( TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN );
}

// If cookies are disabled we can't log in even with a valid password.
if ( isset( $_POST['testcookie'] ) && empty( $_COOKIE[TEST_COOKIE] ) ) {
	$Password_Protected->errors->add( 'test_cookie', __( "<strong>ERROR</strong>: Cookies are blocked or not supported by your browser. You must <a href='http://www.google.com/cookies.html'>enable cookies</a> to use WordPress.", 'password-protected' ) );
}

// Shake it!
$shake_error_codes = array( 'empty_password', 'incorrect_password' );
if ( $Password_Protected->errors->get_error_code() && in_array( $Password_Protected->errors->get_error_code(), $shake_error_codes ) ) {
	add_action( 'password_protected_login_head', 'wp_shake_js', 12 );
}

// Obey privacy setting
add_action( 'password_protected_login_head', 'noindex' );

/**
 * Support 3rd party plugins
 */
if ( class_exists( 'CWS_Login_Logo_Plugin' ) ) {
	// Add support for Mark Jaquith's Login Logo plugin
	// http://wordpress.org/extend/plugins/login-logo/
	add_action( 'password_protected_login_head', array( new CWS_Login_Logo_Plugin, 'login_head' ) );
} elseif ( class_exists( 'UberLoginLogo' ) ) {
	// Add support for Uber Login Logo plugin
	// http://wordpress.org/plugins/uber-login-logo/
	 add_action( 'password_protected_login_head', array( 'UberLoginLogo', 'replaceLoginLogo' ) );
}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>

<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
<title><?php echo apply_filters( 'password_protected_wp_title', get_bloginfo( 'name' ) ); ?></title>
<link rel="stylesheet" href="<?php echo plugins_url('/theme/css/styles.css', dirname(__FILE__)); ?>" type="text/css">
<?php

global $wp_version;

if ( version_compare( $wp_version, '3.9-dev', '>=' ) ) {
	wp_admin_css( 'login', true );
} else {
	wp_admin_css( 'wp-admin', true );
	wp_admin_css( 'colors-fresh', true );
}

if ( $is_iphone ) {
	?>
	<meta name="viewport" content="width=320; initial-scale=0.9; maximum-scale=1.0; user-scalable=0;" />
	<style type="text/css" media="screen">
	.login form, .login .message, #login_error { margin-left: 0px; }
	.login #nav, .login #backtoblog { margin-left: 8px; }
	.login h1 a { width: auto; }
	#login { padding: 20px 0; }
	</style>
	<?php
}

do_action( 'login_enqueue_scripts' );
do_action( 'password_protected_login_head' );
?>

</head>
<body class="login login-password-protected login-action-password-protected-login wp-core-ui">

<div id="login">
	<h1>
		<a href="<?php echo esc_url( apply_filters( 'password_proteced_login_headerurl', 'http://www.thinkaquamarine.com' ) ); ?>" title="<?php echo esc_attr( apply_filters( 'password_protected_login_headertitle', get_bloginfo( 'name' ) ) ); ?>">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 1000" width="80" height="80" fill="#00caa9" style="display:block;margin:0 auto;"><g fill-rule="nonzero"><path d="M480.2 940.8c-54-3.2-98.3-12.8-143.5-31-35.8-14.3-68.4-32.6-99.6-56-52-39-95.4-89.5-125.8-146.6-9-16.8-21.6-45.4-25.5-57.4l-2.7-8c-12.5-35.5-21-80.2-22.8-121l-.3-7.3 4.6-3.4 6.3-4.3c7.8-6 19.6-14 20-14 .3.2.7 8.5 1 18.4.7 41.5 7.4 79.7 20.6 119.8 11.2 34 28 68.2 48 98.2C190 771.5 228 809.7 271 838.8c55.8 38 119.2 61 185.4 68 54.7 5.7 108.5 1 160.7-14.3 89.8-26 170.2-85.3 223.3-164.5 9.2-13.8 15.8-25 23.8-40.7 10.7-20.7 17.7-37.3 24.4-57.5 13.2-40 20-78.3 20.7-119.7.3-9.8.7-18 1-18.2.4-.2 12.2 8 20 14 1 .5 3.8 2.5 6.4 4.3l4.7 3.5-.2 7.2c-2 41-10.3 85.6-22.8 121l-2.7 8c-4 12-16.5 40.7-25.5 57.5-13.5 25.3-29.6 49.4-48 72-33 40.3-74 75.6-118.8 102-42.6 25-89.4 42.6-137.7 52-17 3-27.3 4.6-47.7 6.5-9.3 1-48.8 1.6-57.4 1z"/><path d="M498 898c-2-3.7-4.2-9.6-10.3-26.8-5.3-15-9.8-23.6-17-32.8-17.8-23-50.6-38.5-100.6-47.4-18.4-3.3-27.4-5-37.4-7.8-43.3-11.6-77-29.8-101.5-55-11.7-12.3-20.2-24-26.7-37.3-11-22.4-15.4-44-14.4-69.5.7-20.3 5.8-41.6 14.3-60.6 1.6-3.7 2.7-6.8 2.5-7 0-.4-1.4-1-3-1.5-4.6-1.7-15.7-6.7-16.5-7.5-.7-.7 0-2 3.8-7.8l28.5-43.4c3-5 9.4-14.6 14-21.6l8.3-12.7 20.5-10.2c13.7-7 20.5-10 20.7-9.5.3 1 2.8 20.2 4.4 34.6l1 9.5-1.5 6.6-14 54.3-6.7 26.4c-.7 3-1.5 5.7-2 6.2-.7 1-1.2.8-12-4l-9-4-1 2c-2.2 5.3-6 16.6-8 24.8-5.5 22.5-5.2 43.4 1 60 5.4 14.8 14.6 27.7 26.4 37.2 27.2 22 66.7 34.8 125 41 15.6 1.5 29.2-1.3 40.8-8.6 3.8-2.3 11-9.8 14-14.3 6.2-9.5 11-21.8 14.6-38l1.7-7.6.2-97c0-60.5 0-100.6-.5-106.2-1-13.6-2.6-17.7-8.4-20.8-2.4-1.3-3-1.4-16.2-1-26.7.5-57.2.2-64-.6-34.4-4.2-60.5-12.5-87-27.5-16.7-9.4-36-24.5-51-39.8l-5.7-6-2.2 2c-1.3 1-5 4.3-8 7-17 15.3-44 37-59.4 47.2-6.6 4.5-21.4 14-22.3 14.5-.2 0-3.8 2-8 4.5-9.3 5.6-23 12.8-43 23-8.5 4.2-17.3 8.8-19.4 10l-4 2-3.5-3.7c-4.5-4.8-6.5-7.3-11-14.5-3.8-6-11.4-21-15-29.6-7.5-18.4-19-54-18-55.2.4-.3 3.3-.2 6.6.2 3.3.3 11.4.6 18 .6 42.6 0 90.5-12.7 138-36.4 6.2-3.4 12.5-6.6 14-7.4 10-5.7 13.2-7.5 19.6-11.4 4-2.4 11-7 15.4-10 4.5-3 8.3-5.4 8.5-5.4.2 0 1.6 1.7 3 4 12.7 18 30.6 36.6 48 49.6 11 8.3 20.7 14 33.4 20.4 13.8 6.8 26 11.2 39.8 14.7 13.2 3.3 22 4.4 37 4.8 23 .6 41.3-3 59-11.5l7-3.5-.3-14.7-.3-14.8-8-.5-46-3c-20.7-1.3-44.7-3-53.3-3.4l-16.4-1.3c-1-.4-1-49 0-49.4.7-.2 37.5-2.7 92-6.2l28.5-1.8 3.6-.4v-27.2l-3.8-2.6c-8.2-5.3-17.4-14.6-23-23.2-6.5-9.7-10-18-12.2-28.7-2.8-13.5-2.6-24.7.7-38 6.3-26 26-48 51.4-57.7 10-3.8 17-5 29.5-5 12.6 0 19.5 1.2 29.4 5 25.3 9.7 45 31.8 51.3 57.6 3.3 13.3 3.5 24.5.7 38-2.2 10.8-5.7 19-12 28.8-5.8 8.6-15 18-23 23.2l-4 2.6v27.2l3.6.4c2 0 14.8 1 28.5 2 54.5 3.4 91.3 6 92 6 .6.3.8 6 .8 24.8 0 19-.2 24.5-.8 24.7-.4 0-7.8.7-16.4 1.2-8.5.4-32.6 2-53.4 3.4-20.7 1.3-41.3 2.6-45.8 3l-8 .4-.3 14.8v14.7l7 3.5c17.5 8.5 35.8 12 59 11.5 14.7-.4 23.6-1.5 36.8-4.8 25.6-6.4 51-18.5 73.3-35 17.4-13 35.3-31.7 48-50 1.4-2 2.8-3.7 3-3.7l8.4 5.5 15.4 10c6.4 3.8 9.6 5.6 19.7 11.3l14 7.3c47.4 23.6 95.3 36.3 137.8 36.3 6.7 0 14.8-.3 18-.6 3.4-.4 6.3-.5 6.6-.2 1 1.2-10.5 36.8-18 55.2-3.6 8.5-11.2 23.6-15 29.6-4.5 7.2-6.5 9.7-11 14.5l-3.7 3.8-4-2c-2-1.3-10.7-6-19.3-10.2-20-10-33.7-17.3-43-23l-8-4.5c-1-.5-15.8-10-22.4-14.6-15.4-10.3-42.5-32-59.4-47-3-3-6.7-6-8-7L776 367l-6 6c-15 15.3-34 30.4-50.7 39.7-26.6 15-52.7 23.2-87 27.4-7 .8-37.4 1-64 .5-13.3-.4-14-.3-16.4 1-6 3-7.6 7.2-8.5 20.8-.4 5.6-.6 45.7-.4 106.2l.3 97 1.6 7.6c5 22 11.4 36.3 21.3 46.2 9 9.2 20.6 14 35.2 15 13.7 1 50.3-4.6 75-11.3 42.8-11.5 71.8-31 84.7-56.7 8.7-17.3 11-32.8 8.2-56-1-9.7-6.6-29.8-10.8-39.2l-1-2-8.8 4c-11 4.8-11.4 5-12.2 4-.4-.5-1.2-3.2-2-6.2-.6-3-3.6-14.8-6.6-26.5l-14-54.4-1.5-6.7 1-9.6c1.7-14.4 4.2-33.7 4.5-34.6.2-.5 7 2.6 20.7 9.5l20.5 10.2 8.3 12.7c4.6 7 11 16.7 14 21.6l28.4 43.4c3.7 5.7 4.4 7 3.7 7.8-.8.8-12 5.8-16.6 7.5-1.4.5-2.7 1-2.8 1.5-.2.2 1 3.3 2.5 7 8.5 19 13.6 40.2 14.4 60.5 1 25.5-3.4 47-14.3 69.4-14 28.7-40.6 54.3-74.3 71.8-10.8 5.6-26.2 12-38.7 16-15.2 4.8-28.4 8-52.8 12.2-50 9-83 24.3-100.8 47.4-7 9.2-11.6 18-17 32.8-8 22.8-11.3 30.6-12.7 30.6-.3 0-1.5-1.6-2.5-3.8zm12.8-666.6c9.2-2 19.5-8.4 25.5-15.8 20.6-25.6 9-63.8-22-73.7-4.8-1.6-6.4-1.8-13.8-1.8s-9 .2-13.7 1.7c-22.7 7-36 29.7-31.5 53.2 2 10.6 8.6 21.6 16.7 27.6 5 3.7 12.8 7.5 18 8.5 5.3 1.2 15.4 1.2 20.8 0z"/><path d="M373 593.4c-.5-.7.4-6 5-29.8 2-11.3 5-26.5 9.5-51.5l5-26.4 3-16.3.5-3 4.6-.5c2.6-.2 12-.4 21-.5h16.5l.6 1.4c.4.8 1 10.3 1.7 21 .5 11 2 38.8 3.5 62 1.3 23.5 2.2 42.7 2 43-1 .8-72.4 1.5-73 .7zM575 593.5c-10.5 0-19.5-.4-19.8-.8-.2-.2.7-19.5 2-43 1.4-23.2 3-51 3.5-62 .6-10.7 1.3-20.2 1.7-21l.5-1.4h16.3c9 0 18.5.3 21 .5l4.7.4.5 3 3 16.4 5 26.5 9.6 51.6c4.6 23.5 5.5 29 5 29.7 0 .4-6.4.6-17 .5l-36-.3zM342.3 590.2c-20.8-3-54-7.4-57.7-8-1.6 0-3.2-.3-3.6-.4-.8-.3 1.6-7.3 17-52l7.5-21.7 7-21c3.3-9 7-20.2 8.7-24.8 1.5-4.7 3-8.7 3.2-9 .2 0 7 1 15 2.8 8 1.6 17.2 3.5 20.3 4 3 .7 5.6 1.5 5.8 2 .2.4-.7 24-2 52.4-1 28.4-2.4 57.4-2.6 64.4-.3 8-.7 12.8-1.2 13.2-.4.3-7.7-.5-17.5-1.8zM641 591.8c-.3-.4-.7-6.3-1-13.2l-2.6-64.2c-1.2-28.4-2-52-2-52.4.3-.5 3-1.3 6-2l20-4c8.2-1.7 15-3 15.2-2.7.2.2 1.7 4.2 3.2 9l8.6 24.8 7 21c1 2.5 4.3 12.3 7.6 22 13.8 39.6 17.7 51.4 17.3 51.7-.2 0-2 .4-4.2.6l-21.7 3L660 590c-9 1-16.8 2.2-17.3 2.3-.6 0-1.3 0-1.6-.5zM161 527.5c-5.5-4-14.3-11-19.7-15.4l-17.5-13.6c-9.2-7-18.4-14.6-18.4-15 0-.3 4-3.6 8.6-7.3l30.4-23.8L155 444c15-12 45.4-36 47.5-37.4 3.4-2.6 6.5-5 13-10.3l6-4.8 1.6 1.4c.8.7 7 7.4 13.4 14.7l12 13.2-2.4 3.6-17.3 25.7-21.6 32c-3.6 5.6-13 19.7-21 31.3-7.7 11.7-14.4 21.3-14.7 21.4-.4 0-5-3.2-10.4-7.5zM828.3 533.5L815 513.7 795 484l-22.7-33.7-17.3-25.7-2.4-3.7 12-13.3L778 393l1.4-1.5 6 4.8 13 10.3c2.2 1.5 32.6 25.4 47.7 37.4l10.6 8.2c2.3 1.7 17.4 13.6 30.4 24 4.7 3.6 8.6 7 8.6 7.2 0 .4-9.2 8-18.4 15L859.7 512c-21 16.7-29.4 23.2-29.8 23.2-.3 0-1-.7-1.7-1.7zM87 347.4c-.5-1 11-28.2 18.5-43.2 11-22 17.7-33.4 34.3-57.8 2.8-4.2 15-20.5 18.3-24.3 26-31 52-55.8 80-76.6 4.2-3 4-3 12.3-8.6C311.5 94.4 382.7 68.2 456 61.2c66.8-6.4 133 1.6 194 23.6 35.2 12.7 69.6 30.4 100.7 52l12.2 8.6c28 20.8 54 45.7 80 76.7 3 4 15.4 20.2 18.2 24.4 23.6 35 37 59.7 51.4 96.5 1 2.4 1.4 4.2 1 4.5-1 1-21.8-5-33-10l-5.6-2.2-2.3-5c-20.6-45-49-85.8-83.5-120.2-15-14.7-28.4-26.5-43-37.3l-9.8-7.2c-1.2-1-5.8-4-10.3-7-56.7-37.4-119.3-59.4-187.6-66-16-1.5-59.7-1.5-75.8 0-48.8 4.7-93.4 17-136.8 37.4-20.5 9.6-47.6 25.3-61.2 35.5-1.2 1-5.6 4-9.7 7.2-52 38.4-95.5 91.8-123.7 151.3-3.7 7.6-5.7 11.2-6.7 11.8-3.7 2-15 6-24.5 9-10.2 3-12.4 3.5-13 2.6z"/></g></svg>
		</a>
	</h1><!-- aquamarine -->
	<?php

	// Add message
	$message = apply_filters( 'password_protected_login_message', '' );
	if ( ! empty( $message ) ) {
		echo $message . "\n";
	}

	if ( $Password_Protected->errors->get_error_code() ) {
		$errors = '';
		$messages = '';
		foreach ( $Password_Protected->errors->get_error_codes() as $code ) {
			$severity = $Password_Protected->errors->get_error_data( $code );
			foreach ( $Password_Protected->errors->get_error_messages( $code ) as $error ) {
				if ( 'message' == $severity ) {
					$messages .= '	' . $error . "<br />\n";
				} else {
					$errors .= '	' . $error . "<br />\n";
				}
			}
		}
		if ( ! empty( $errors ) ) {
			echo '<div id="login_error">' . apply_filters( 'password_protected_login_errors', $errors ) . "</div>\n";
		}
		if ( ! empty( $messages ) ) {
			echo '<p class="message">' . apply_filters( 'password_protected_login_messages', $messages ) . "</p>\n";
		}
	}
	?>

	<?php do_action( 'password_protected_before_login_form' ); ?>

	<form name="loginform" id="loginform" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="post">
		<p>
			<label for="password_protected_pass"><?php _e( 'Password', 'password-protected' ) ?><br />
			<input type="password" name="password_protected_pwd" id="password_protected_pass" class="input" value="" size="20" tabindex="20" /></label>
		</p>
		<!--
		<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="90"<?php checked( ! empty( $_POST['rememberme'] ) ); ?> /> <?php esc_attr_e( 'Remember Me', 'password-protected' ); ?></label></p>
		-->
		<p class="submit">
			<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e( 'Log In', 'password-protected' ); ?>" tabindex="100" />
			<input type="hidden" name="testcookie" value="1" />
			<input type="hidden" name="password-protected" value="login" />
			<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $_REQUEST['redirect_to'] ); ?>" />
		</p>
	</form>

	<?php do_action( 'password_protected_after_login_form' ); ?>

</div>

<script type="text/javascript">
try{document.getElementById('password_protected_pass').focus();}catch(e){}
if(typeof wpOnload=='function')wpOnload();
</script>

<?php do_action( 'login_footer' ); ?>

<div class="clear"></div>

</body>
</html>